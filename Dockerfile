FROM resin/rpi-raspbian

# File Author / Maintainer
MAINTAINER Jrgen Blakstad

# Update the sources list
RUN apt-get update

RUN apt-get install -y python3 python3-pip python3-doc python3-dev

# Install django
RUN pip3 install django
RUN pip3 install dj-static
RUN pip3 install static3
RUN pip3 install wheel

# Install redis, celery
RUN apt-get -y install redis-server
RUN service redis-server start
RUN pip3 install celery[redis]
RUN pip3 install django-celery-results

RUN apt-get install -y apt-utils

# Install basic applications
RUN apt-get install -y vim tar git curl nano wget net-tools build-essential telnet openssh-client less netcat nmap

# Install python git libraries
RUN pip3 install gitdb
RUN pip3 install nose
RUN pip3 install -U mock
RUN pip3 install gitpython

# Install Netmiko
# https://pynet.twb-tech.com/blog/automation/netmiko.html
RUN apt-get install -y libffi-dev libssl-dev python3-yaml
RUN pip3 install pycrypto
RUN pip3 install utils
RUN pip3 install --upgrade cffi
RUN pip3 install --upgrade cryptography
RUN pip3 install netmiko

# Install node 7
RUN curl -sL https://deb.nodesource.com/setup_7.x | bash -
RUN apt-get install -y nodejs
RUN npm install -g npm-autoinit
RUN npm config set onload-script npm-autoinit/autoinit

# Install ciscoparse
# https://www.npmjs.com/package/ciscoparse
RUN npm install ciscoparse

# Install ciscoconfparse (Python tool to parse cisco config)
RUN pip3 install --upgrade ciscoconfparse

# Install Butterfly (xterm in browser)
RUN pip3 install butterfly
RUN pip3 install libsass

# Install openssh
RUN apt-get -y install openssh-server
RUN perl -pi -e 's/PermitRootLogin without-password/PermitRootLogin yes/g' /etc/ssh/sshd_config

RUN apt-get install -y libjpeg-dev

# Install filebrowser
RUN pip3 install django-grappelli
RUN pip3 install Pillow
RUN pip3 install django-tinymce
RUN pip3 install django-filebrowser

ENTRYPOINT service redis-server start && bash

EXPOSE 80 22 57575
